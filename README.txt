********************************************************************
D R U P A L    M O D U L E
********************************************************************
Name: nodetaxonomy_notifications
Authors: Peter Brownell and Russell Blakeborough
Sponsors: Code Positive [www.codepositive.com] 
          and school of everything [www.schoolofeverything.com]
Drupal: 5.x
********************************************************************
DESCRIPTION:

This module is a notifications plugin that uses the terms assigned 
to a node as the basis for updates. The initial implementation has been 
designed to work with nodeprofile to allow users to tag themselves
and then get told about new stuff. 

One of our objectives for this was to not need to use the standard
notifications_ui, so, we have not yet tested integration with that
module. 


********************************************************************
INSTALLATION:

  Note: It is assumed that you have Drupal up and running.  Be sure to
    check the Drupal web site if you need assistance.  If you run into
    problems, you should always read the INSTALL.txt that comes with the
    Drupal package and read the online documentation.

Install the Notifications / Messaging framework:
http://drupal.org/project/notifications

Install this module & enable it.


********************************************************************
CONFIGURATION:

Visit the admin page -> admin/settings/nodetaxonomy_notifications 
and do a little config. 

-- Auto Types
Select which types should automatically subscribe their owner
to updates based on node taxonomy. 

Since this was designed for node profiles, by enabling a node profile
as an auto types, a user will atomatically be subscribed to terms
on their profile.  

The initial subscription settings are notification module defaults.

-- Trigger Types
Which types should trigger updates. You will not necessarily want to
tell users about every node update, so select the ones you want here. 

-- Block Display types
If you want to use the main subscription management blocks, you can
tell them to appear on specific types of nodes. Select which types
to activate it for here. 
  
DEIFINING YOUR UPDATE TEXT
You will probably need to do some localisation to fix the update
text. The strings are defined in nodetaxonomy_notifications_notifications, 
but it's best to use locale to do some updates. 

This initial realease has been genericised from our internal code, 
so the default messages me be a little messy. 


DEBUGGING
There is a debug block that will produce a pretty raw dump of 
subscription info for a node. This will tell you if you are subscribed,
or whether or not your user would receive an update if that node was
modified. 
       
      
********************************************************************
ACKNOWLEDGEMENTS:

Thanks to Jose Reyero and the Notifications team for framework that 
makes this possible.

