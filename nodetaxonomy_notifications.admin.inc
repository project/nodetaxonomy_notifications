<?php

/**
 * @file
 * Include file for nodeaxonomy_notifications admin interface
 * 
 */




//////////////////////////////////////////////
// Admin form                        /////////
//////////////////////////////////////////////


/**
 * Admin setting form.
 *
 * @return 
 *   Form definitions
 */
function nodetaxonomy_notifications_admin_form() {
  //TODO: These forms need a little more copy to explain the options
  // node profile is installed
  //TODO: allow selection of profile type for default nid
  if (module_exists('taxonomy_nco')) {
    $form['nodetaxonomy_notifications_nco_enabled'] = array(
      '#title' => t('Enable NCO'),
      '#type' => 'checkbox',
      '#description' => 'Enable/Disable notifications for similar terms using Taxonomy NCO',
      '#default_value' => variable_get('nodetaxonomy_notifications_nco_enabled', FALSE),
    );
    $form['nodetaxonomy_notifications_minimum_match_nco'] = array(
      '#title' => t('NCO cutoff level'),
      '#type' => 'textfield',
      '#description' => 'Terms must have an NCO relationship greater than this to trigger notifications',
      '#default_value' => variable_get('nodetaxonomy_notifications_minimum_match_nco', NODETAXONOMY_NOTIFICATIONS_DEFAULT_MINIMUM_MATCH_NCO),
    );
  }
  /*
  $form['nodetaxonomy_cron'] = array(
      '#title' => t('Run updates on cron'),
      '#type' => 'checkbox',
      '#description' => 'Process notifications triggers on cron',
      '#default_value' => variable_get('nodetaxonomy_cron', FALSE),
  
    );
    */
  $node_types = node_get_types($op = 'types');
  foreach ($node_types as $key => $type) {
    $type_options[$type->type] = $type->name;
  }
  $form['nodetaxonomy_notifications_auto_types']=array(
    '#type' => 'checkboxes',
    '#title' => t('Automatically enable subscriptions for these node types'),
    '#options' => $type_options,
    '#default_value' => variable_get('nodetaxonomy_notifications_auto_types', array()),
    '#description' => t(
      'When a user creates a node of one of these types, they will automatically be subscribed to it\'s terms.
      Users could, for example, be subscribed to any terms on their node profiles. 
      Subscriptions are not currently applied retroactively. 
      (Although the code is in the module, _nodetaxonomy_notifications_enable_for_node_type, 
      I still need to do a little more testing before I turn it on. Sue me.).'),
  );
  
  $form['nodetaxonomy_notifications_active_types']=array(
    '#type' => 'checkboxes',
    '#title' => t('Trigger notification events for these node types'),
    '#options' => $type_options,
    '#default_value' => variable_get('nodetaxonomy_notifications_active_types', array()),
    '#description' => t('Select which types of nodes users should be kept informed about.'),
     
  );
  $form['nodetaxonomy_notifications_allowed_types']=array(
      '#type' => 'checkboxes',
      '#title' => t('Display notifications management blocks on pages for these types'),
      '#options' => $type_options,
      '#default_value' => variable_get('nodetaxonomy_notifications_allowed_types', array()),
  );

  return system_settings_form($form);
}

